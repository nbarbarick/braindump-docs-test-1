# Welcome to MkDocs


Here's a code block:

```
INFO     -  [20:19:31] Reloading browsers
INFO     -  [20:19:31] Browser connected: http://127.0.0.1:8000/bash-1/
INFO     -  [20:19:34] Detected file changes
INFO     -  Building documentation...
``` 

## _What is the shell_?

The shell is an interactive command-line interpreter. The shell is a process. The shell provides a wrapper for commands (does this mean that you get to just use the command, and the code behind it is abstracted away?)

The shell is built into the operating system. Shell scripts are the glue that connects the pieces of the development process (toolchains, integration, building, testing).

Its prime mover tools: 

- awk
- sed

These are tools to process input into the desired output. More about them later.

You can also use the shell to tweak configurations to make the kind of environment you want.

## _Login Shell_

The sshd daemon is a process listening for users who are trying to log in. It checks their credentials or ssh keys, then if these are correct, sshd fires off a bash (ie it spawns a child process), which is a login _and_ interactive shell.

If you then ran the bash command with this shell, that would be a child process of the login shell. It would not, itself, be a login shell but it would be an interactive shell.

What's the difference?

The login shell has the user's environment set up, based on environment variables. All programs you run from the login shell inherit the values of the environment variables. Environment variables, however, do NOT propagate backward to parent processes. Inheritance only goes in one direction.

## _Environment Variables_ 

