# Braindocs: The Braindump Documentation Site

Welcome to the placeholder text for the Braindocs documentation site. This site is built using [MkDocs](https://www.mkdocs.org/), a static site generator that uses Markdown files to build a static website.

A few pages have been added to the navigation demonstrate the basic architecture and functionality.

## Goals of the project

Information about this site is available on the [contribute](contribute-to-docs.md) page.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In fermentum posuere urna nec. 

Eget egestas purus viverra accumsan in. Gravida dictum fusce ut placerat orci nulla pellentesque dignissim. Vulputate eu scelerisque felis imperdiet proin fermentum leo vel. Eget dolor morbi non arcu risus quis varius quam. Augue ut lectus arcu bibendum. Et magnis dis parturient montes nascetur ridiculus mus. 

Et malesuada fames ac turpis egestas maecenas pharetra. In fermentum et sollicitudin ac orci phasellus egestas tellus rutrum. Adipiscing vitae proin sagittis nisl rhoncus mattis. 

Varius vel pharetra vel turpis. Id velit ut tortor pretium viverra suspendisse potenti nullam. Platea dictumst quisque sagittis purus sit amet volutpat consequat mauris. Mattis ullamcorper velit sed ullamcorper morbi tincidunt. 

Nulla facilisi nullam vehicula ipsum a arcu cursus. Purus gravida quis blandit turpis cursus. Tempus quam pellentesque nec nam. Egestas diam in arcu cursus euismod quis viverra. Ac tincidunt vitae semper quis lectus.

![Alt text](images/BrainDump.png)

Arcu cursus vitae congue mauris rhoncus aenean vel elit. Massa placerat duis ultricies lacus sed. Tortor at auctor urna nunc. Porttitor massa id neque aliquam vestibulum morbi. Mi sit amet mauris commodo quis imperdiet massa tincidunt nunc. 

Turpis cursus in hac habitasse platea dictumst quisque sagittis. Amet risus nullam eget felis eget. Amet porttitor eget dolor morbi non arcu risus quis. Volutpat odio facilisis mauris sit amet massa vitae tortor condimentum. 

Fusce id velit ut tortor pretium viverra suspendisse. Tristique senectus et netus et. Commodo elit at imperdiet dui accumsan sit amet. Sit amet nisl purus in mollis. 

Eget aliquet nibh praesent tristique magna sit amet purus gravida. Vitae aliquet nec ullamcorper sit amet risus nullam. Rutrum quisque non tellus orci ac. 

At consectetur lorem donec massa sapien faucibus. Donec et odio pellentesque diam. Aliquet bibendum enim facilisis gravida neque convallis. At varius vel pharetra vel turpis nunc eget lorem dolor.
## Lorem II

Rhoncus mattis rhoncus urna neque viverra justo. Adipiscing elit ut aliquam purus sit amet luctus. Amet porttitor eget dolor morbi. Dictum at tempor commodo ullamcorper. 

Leo in vitae turpis massa sed elementum tempus egestas sed. Ut placerat orci nulla pellentesque dignissim enim sit amet venenatis. Mollis aliquam ut porttitor leo a diam sollicitudin. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim. 

Nisl pretium fusce id velit ut tortor pretium viverra suspendisse. Tincidunt praesent semper feugiat nibh. Nisi quis eleifend quam adipiscing vitae proin sagittis nisl. 

Duis at tellus at urna condimentum mattis. Nullam non nisi est sit amet facilisis magna. Cursus in hac habitasse platea. Malesuada fames ac turpis egestas sed tempus urna. Amet facilisis magna etiam tempor orci eu lobortis elementum. Iaculis eu non diam phasellus vestibulum lorem. Scelerisque purus semper eget duis. Faucibus a pellentesque sit amet porttitor eget. Viverra mauris in aliquam sem fringilla.

## Lorem III

Turpis nunc eget lorem dolor sed viverra. Sollicitudin nibh sit amet commodo nulla facilisi. Hac habitasse platea dictumst vestibulum rhoncus. Tellus mauris a diam maecenas. Vitae congue eu consequat ac felis donec et. Malesuada bibendum arcu vitae elementum curabitur vitae nunc sed velit. Consequat ac felis donec et odio pellentesque. Turpis egestas pretium aenean pharetra magna ac placerat. Sit amet nisl suscipit adipiscing bibendum est ultricies. At elementum eu facilisis sed odio morbi quis commodo odio.

Arcu non sodales neque sodales ut etiam sit amet nisl. Accumsan tortor posuere ac ut consequat semper viverra nam. Eu feugiat pretium nibh ipsum consequat nisl vel. Tellus at urna condimentum mattis. Vulputate odio ut enim blandit volutpat maecenas. Nullam non nisi est sit amet. Amet mattis vulputate enim nulla. Proin sagittis nisl rhoncus mattis rhoncus urna neque. Ornare arcu dui vivamus arcu felis bibendum ut tristique. Facilisis sed odio morbi quis commodo odio aenean sed. Commodo quis imperdiet massa tincidunt nunc pulvinar sapien et ligula.
