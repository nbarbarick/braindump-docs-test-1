# Contributing to Braindocs

Welcome to the exciting world of Braindocs. We're glad you're here. 

To become a contributor to this site, please read this page and get in touch with one of the Braindocs project leads.

## What is Braindocs?

Braindocs is a collaboratively built documentation site for the Computer Action Team's weekly Braindumps. 

It serves as a written record of the subjects presented at Braindumps, and as a learning supplement for CATs to dive deeper into topics presented at these sessions.

Technically speaking, Braindocs is a static website created with a static site generator called [MkDocs](https://www.mkdocs.org/). We are hosting it on the CAT's self-managed GitLab instance and deploying it to a subdomain of the CAT's website.

## Why should I Braindoc?

There are at least a few benefits for you when contributing to Braindocs:

* You will learn how to write in Markdown, which is used in many places on the internet, including GitHub, GitLab, Reddit, and Discord.

* You will learn how to collaborate using ```git```, a version control software that is standard in the realms of software development and IT.

* You have the opportunity to learn how to quickly build and deploy a documentation site for your cool new project.

* You will have the chance to dive deeply into the subject matter presented during Braindumps. 

* You will have the chance to develop skills related to:

    * Asking good questions
    * SME interviews
    * Information architecture
    * Visual communication

* Perhaps most importantly, you will work on something that will be a useful learning resource for your fellow CATs, for centuries to come.

## How is the site made?

Braindocs is a collection of markdown files that are compiled into a static website using [MkDocs](https://www.mkdocs.org/).

## How do I contribute?

See the [project README]() for steps on contributing to the site.

More specific information on changing the site style, adding images, new functionality, or changing the site structure is forthcoming, once this project gets a green light.