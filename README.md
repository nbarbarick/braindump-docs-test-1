# Braindocs (Test)

The site is here: https://nbarbarick.gitlab.io/braindump-docs-test-1/

To build it locally, you must first have Mkdocs and its Material theme installed.

To install Mkdocs, see here: https://www.mkdocs.org/getting-started/

To install the Material theme, see here: https://squidfunk.github.io/mkdocs-material/getting-started/

Once you have Mkdocs and the Material theme installed, clone this repository and build the site locally.

Clone by running this in your terminal:

```
git clone https://gitlab.com/nbarbarick/braindump-docs-test-1.git
```
Change directories into braindump-docs-test-1:

```
cd braindump-docs-test-1
```
Build the docs:

```
mkdocs build
```
Open the site in your browser:

```
mkdocs serve --livereload
```

This will cause several lines of output to appear in the terminal. 
About 5 lines down, it will say:

    _Serving on http://127.0.0.1:8000/_

This is the address where you can view a local version of the site, where your local changes will show up.

You can now make changes to the files and see most of them take effect in the browser. If you add plugins or changes some of the file structure, you will need to end local build by entering ```Ctrl+C``` in your terminal, then building again and then serving again.
